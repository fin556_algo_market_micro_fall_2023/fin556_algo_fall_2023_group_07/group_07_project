# group_07_project

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Teammates

**Xiaoyue Chen** (xc53@illinois.edu)

- I am Xiaoyue Chen, currently pursuing my Master's in Financial Engineering at the University of Illinois Urbana-Champaign. During my undergraduate period I start to get interested in the financial market and open my own trading account and observe the market movement every day. I decide to pursue the career as a Quantitative Researcher/Data Analyst after graduation.During my internship I did many researches about high frequency trading, especially on time series limit order book and order flow. I am familiar with the data analysis with the high frequency trading data.

My linkedin webpage: https://www.linkedin.com/in/xiaoyue-chen-93719b178/

My github webpage: https://github.com/chengxiaoyueqaz

**Saranpat Prasertthum (sp73@illinois.edu)**

- My name is **Saranpat Prasertthum**, and I am currently pursuing my **Master's in Financial Engineering** at the University of Illinois at Urbana-Champaign, with an expected graduation in December 2023. My academic journey has equipped me with knowledge in High Frequency Trading, Stochastic Calculus, and Algorithmic Trading. Professionally, I spent **two years as a Software Engineer** at Blockfint, a startup company. I am proficient in languages such as **Python, C++, Go, Java, and JavaScript, and have a strong background in SQL**. Additionally, I have hands-on experience as a **backend developer** and am well-versed in cloud platforms. My areas of interest encompass **high-performance computing, finance, and trading**. My Linkedin is located at: **https://www.linkedin.com/in/saranpatp**

**Savya Raghavendra (raghavsavya@gmail.com)**

- I am **Savya Raghavendra**, and I am pursuing my **Bachelor's in Engineering Physics** at the University of Illinois at Urbana-Champaign. I have been trading for the last 3 years, and have been interested in algorthimizing strategies. I am currently leading an algorithmic trading team at Illini Blockchain, where we focus more on swing trading strategies, and I am very interested in the high frequency/low latency side of trading as well. I have professional experience as an AI/Engineering Intern at Crucial Bits. I am proficient in Python and Java, and C++. I am interested in **quantative research, trading, and in finance as a whole**.

My Linkedin is [here](https://www.linkedin.com/in/savya-raghavendra-503a88263/)

**Nicholas Messina (messina4@illinois.edu)**

- I am **Nick Messina**, and I am pursing my **BS/MCS in Computer Science** at the University of Illinois at Urbana-Champaign, with an expected graduation in May 2025. Through my academic career I have grown interests in Machine Learning, Artificial Intelligence, and High Frequency Trading. Professionally, I have experience as a **Software Engineering Intern** at Motorola Solutions and CapitalOne. I am proficient in **Python, C++, C, Java, R, SQL**. I am interested in positions in the high frequency trading industry.

My Linkedin is [here](https://www.linkedin.com/in/nick-messina-2035111ba/).  
My GitHub Profile is [here](https://github.com/Messina22)

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_07/group_07_project.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_07/group_07_project/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

---

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name

Choose a self-explaining name for your project.

## Description

Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

For open source projects, say how it is licensed.

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
