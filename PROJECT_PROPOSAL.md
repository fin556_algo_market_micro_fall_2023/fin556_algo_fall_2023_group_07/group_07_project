# **Project Proposal: Market Making Strategy in Crypto Market**

## **1. Technical Stack**

### **Languages:**
- Python
- R
- C++

### **Technologies:**
- Raspberry Pi
- Computers (Mac and Windows)

### **Frameworks:**
- Strategy Studio

### **Libraries:**
- Pandas
- Pickle
- Numpy
- Keras
- Tensorflow
- Sklearn
- Matplotlib
- Itertools
- Pyfolio
- Backtrader
- Datetime
- Seaborn

## **2. Trading Assets**

- Primary focus: Cryptocurrencies with a special emphasis on Bitcoin.

## **3. Data Sources**

- Primary: Binance, Kaggle, In house data mining
- Secondary: Provided by Professor

## **4. Backtesting**

- **Date Range:** January 2022 to February 2022
- **Rationale:** For high-frequency strategies, a one-month backtest is sufficient to determine the viability of the trading strategy.

## **5. CI/CD**
We will use gitlab CI/CD if possible

## **6. Trading Strategy**

- **Type:** Market Making Strategy
- **Objective:** Profit from the spread of BBO (Best Bid and Offer).
- **Risk Management:** Purchase put futures on assets to mitigate inventory risk.
- **Position Management:** Positions will be held overnight.

## **7. Project Timeline**

1. **Weeks 1-2:** Literature review, data procurement, EDA framework setup, and understanding Binance's fee structure.
2. **Weeks 3-4:** Implementation of theories and concepts from literature. Strategy filtering.
3. **Weeks 5-6:** Original research and market behavior analysis.
4. **Weeks 7 onwards:** Strategy deployment, backtesting using Strategy Studio, report writing, presentation preparation.

## **8. Milestones**

- **Minimum Target:** Successful backtest with calculated return rate, max drawdown, and Sharpe ratio.
- **Expected Completion:** Ensure the strategy is profitable.
- **Reach Goals:** Discovering and implementing advanced strategies for greater profitability.

## **9. Team Roles and Contribution**

Post-framework completion, all team members will focus on research and strategy development, aiming to identify and capitalize on the market's "real alpha."

## **10. External Resources**

- Tick data from various cryptocurrency exchanges.

## **11. Project Deliverables**

- A viable and profitable high-frequency trading strategy.

## **12. Team Graduation Dates**

- **Saranpat Prasertthum:** December 2023
- **Savya Raghavendra:** May 2025
- **Xiaoyue Chen:** May 2024
- **Nick Messina:** May 2025
