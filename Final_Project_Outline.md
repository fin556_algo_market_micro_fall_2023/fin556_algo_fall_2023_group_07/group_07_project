## Introduction

In the new and rapidly evolving world of cryptocurrency trading, knowing how to navigate this volatile market as opposed to traditional markets is crucial. This project focuses on the creation and backtesting of a market making algorithm made for the cryptocurrency market. Unlike traditional stock markets, the crypto market is characterized by unique differences and opportunities: its volatility, 24/7 market hours, and a landscape influenced differently by retail and institutional investors. These distinct features are interesting to take on with a market making strategy.

The primary goal of this algorithm is to generate profit. This objective, while obvious, is complicated by the unpredictability and liquidity variations in the crypto market. We are testing various models and algorithms, to find out which holds best in the market's unique characteristics.

Our approach also includes extensive backtesting across a spectrum of market conditions, going beyond the typical bull and bear market scenarios. For example, we backtested with 2020 data, during COVID, which contained most irregular price movements within a market, amplified in cryptocurrency. This testing is essential to test for the algorithm's robustness and reliability across different market phases. Another significant part of this project involves the application of data science skills: acquiring, extracting, and formatting trading data from cryptocurrency exchanges like Gate.io for use with Strategy Studio software. This process not only tests our development skills but also in handling and analyzing real-world financial data.

This paper will cover several key areas:

- **Background**: We will start by explaining what market making is, how it generates profit, and foundational elements of what is involved.
- **Different Models**: Various market making algorithms will be analyzed, and tested within the market.
- **Data Collection**: The methodology for sourcing and preparing data from cryptocurrency exchanges will be detailed.
- **Backtesting**: We will outline our approach to backtesting the developed algorithms.
- **Final Results/Analysis**: The outcomes of our backtesting will be presented and analyzed, evaluating the performance of the algorithms.
- **Conclusion**: The paper will conclude with a summary of our findings, the contributions of our project to the field of cryptocurrency trading, and potential avenues for future research.

---

### Background

#### General Concept of Market Making

Market making is an essential function in financial markets, by continously providing buy and sell orders to facilitate trading. Market makers add liquidity to the market, which is essential for ensuring active trading and price stability. By offering to buy and sell securities/assets, they bridge the gap between buyers and sellers.

#### Liquidity

Liquidity refers to the ease with which assets can be bought or sold in a market without causing a significant movement in the price. In the context of market making, liquidity is important for minimizing trading costs and keeping efficient market operations. Market makers play a key role in maintaining this liquidity. By always being ready to buy or sell, they ensure market participants can execute trades while keeping the price stable.

#### Order Books

An order book is a list of buy and sell orders for a specific asset, organized by price level. Market makers contribute to these books by placing orders.
<img src="Order.png" alt="Semantic description of image" width="300"/>

For example, in this order book, if someone places a buy order at $100.02 for 800 shares, it will execute at $100 and 1000 shares will become 200. Another example is if someone places a sell order at market price for 160 shares, 100 shares will execute at $99.95, 50 will execute at $99.90, and 10 will execute at $99.85.

https://www.youtube.com/watch?app=desktop&v=Kl4-VJ2K8Ik

#### Bid/Ask Prices

The bid price is the highest price a market maker is willing to pay to buy an asset, while the ask price is the lowest price at which they are willing to sell. The difference between these prices, known as the spread, is crucial for a market maker's profitability.

#### Spread

The spread, the difference between the bid and ask prices, is a key element in market making. Market makers earn their profits primarily through capturing this spread. By buying at the bid price and selling at the ask price, they benefit from the price difference between buying and selling orders.

#### Profit Generation and Strategy

Market makers aim to generate profit primarily through the spread. Managing this profitably involves complicated inventory management strategies to mitigate risk. By balancing their buy and sell orders, market makers ensure they are not overly exposed to market movements, which could lead to significant losses. If a market maker has a certain amount of inventory, they might hedge it by buying puts (predicting the market will go down), which ensures they don't lose money if the price goes down dramatically. If the price goes up dramatically, their inventory is the hedge (since it is now worth more) to ensure no loss.

#### Alternative Objectives in Market Making

Not all market making is solely profit-driven. For instance, some hedge funds engage in market making with the objective of increasing trading volume on a particular exchange. This increased volume may lead to better fee arrangements with the exchange, benefitting other trading desks within the firm by reducing overall trading costs.

#### Types of Market Making Strategies

There are various market making strategies, including quote-driven, high-frequency trading (HFT), electronic, and order-driven. This project will mainly focus on HFT strategies, particularly in the context of the cryptocurrency market.

#### Price Discovery

Market makers also aid in the process of price discovery, where the market price of an asset is determined through the interactions of buy and sell orders. In volatile or illiquid markets, market makers play a crucial role in ensuring that prices reflect the current supply and demand dynamics.

### Different Market Making Models

#### Avellanada-Stoikov Model

The Avellaneda-Stoikov market-making model is a significant contribution to the field of quantitative finance, particularly in the area of algorithmic trading. Introduced by Marco Avellaneda and Sasha Stoikov in 2008, this model provides a framework for setting optimal bid and ask quotes in a limit order book market. Here’s more detail: https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_07/group_07_project/-/blob/market_making/AS%20Model%20Research.pdf

we use parameter:

- $s$ the per-unit mid-price of the
- $T$, the terminus of the time series
- $\sigma$, the volatility of the asset
- $q$, the number of assets held in inventory
- $\gamma$, a risk factor that is adjusted to meet the risk/return trade-off of the market maker
- $x$, the initial capital of the market maker
- $k$, the intensity of the arrival of orders

for every seconds, we calculate the reservation price on both sides, when the market price is higher than our ask price, we sell 1 unit, when a market order is lower than our bid price, we buy 1 unit, finally we want to control the inventory batween a certain threshold.

#### Sigmoid Model

A sigmoid function is a mathematical function having a characteristic "S"-shaped curve or sigmoid curve. It's mathematical representation is:

$f(x) = \frac{1}{1 + e^{-x}}$

You can use this function as a reaction to the bid/ask price, and adjust the size of the position you buy/sell.

##### Results

Parameters

- Instrument: SPY
- Date: 2019-01-30
- Position Cap: 5000
- PnL Sharpe Ratio: 0.06548127150741091
  ![2019-01-30-10000-500](./4_SigmoidStrategy/pnl_analysis/PNL_Screenshot.png)


#### Basic Order Flow Imbalance Model

##### Model Summary

- **Definition**: Order flow imbalance refers to a situation where there is a significant difference between the volume of buy orders and sell orders for a financial instrument. This concept is crucial in algorithmic trading and market microstructure analysis because it provides insights into the supply and demand dynamics in the market and can be indicative of future price movements.
- **How it measured**: It is typically measured over a specific time frame, and can be quantified in various ways, such as the difference in the number of buy and sell orders, the total volume of buy vs. sell orders, or the aggregate value of buy and sell orders.

##### Code Explaination

The `OnDepth` event is called whenever there is a change in the orderbook. When this event if called:

- The strategy gets the `best_bid_price` and the `best_ask_price` (NBBO) of the orderbook. It also get the quantity of the orders at those price levels i.e. `best_bid_size` & `best_ask_size`.
- It then calculates `imbalance` as `best_bid_size - best_ask_size`
- If the absolute value of `imbalance` is greater than a user defined `threshold` then:

  - If the `imbalance > 0` i.e. there is an imbalance of more buy orders than sell orders:
    - We create sell orders at the `best_ask_price` to rebalance the orderbook
  - If the `imbalance < 0` i.e. there is an imbalance of more sell orders than buy orders:

    - We create buy orders at the `best_bid_price` to rebalance the orderbook

  - Note: There is a user defined `max_buy_position` and `max_sell_position` to limit the number of orders the strategy creates.

##### Code Reasoning

The strategys reasoning is centered on the role of a market maker in providing liquidity to the market. By responding to changes in the orderbook (via the OnDepth event), the strategy actively manages order imbalances. This is achieved by comparing best_bid_size and best_ask_size to detect imbalances. When an imbalance exceeds a certain threshold, the strategy intervenes by creating orders—selling when there's an excess of buy orders, and buying when there are more sell orders.

The strategy is based on the thought that if there are more buy orders than sell orders, there must be people wanting to buy. Therefore, we create more sell orders since people are looking to buy. Same goes for the opposite. If there are more sell orders than buy orders, people are wanting to sell. So, we create more buy orders since people are looking to sell.

##### Results

#### PnL Plots

Parameters

- Instrument: SPY
- Date: 2019-01-30
- Position Cap: 10000
- Threshold: 500
- PnL Sharpe Ratio: -0.30607

![2019-01-30-10000-500](./0_OfiStrategy/strategy_analysis/plots/pnl-2019-01-30-2019-01-30-10000-500.png)

Parameters

- Instrument: SPY
- Date: 2022-05-02 - 2022-05-06
- Position Cap: 10000
- Threshold: 1000
- PnL Sharpe Ratio: 1.28250

![2022-05-02-2022-05-06-10000-1000](./0_OfiStrategy/strategy_analysis/plots/pnl-2022-05-02-2022-05-06-10000-1000.png)

##### Possible Next Steps for Model

- Add more parameters
  - X number of levels included in imbalance consideration
  - Threshold could become a ratio instead of a fixed value
  - Stop loss becoming a function instead of just a capped position
- Order cancellation when market imbalance naturally corrects
- Hyperparameter tuning for all parameters
- Imbalance in other correlated instruments can be detected to also be factored

#### Avellaneda Stoikov Model

##### Model Summary

for every second we calculate the reservation price based on the midprice and 5 other parameters, after that when the lowest price of that second lower than the bid price we buy 1 unit and when the highest price of that second higher than the ask price we sell 1 unit and keep the inventory within the MAX_POSITION.

##### Results

Parameters

- Instrument: SPY
- Date: 2019-01-30
- Position Cap: 10000
- PnL Sharpe Ratio: 0.9403262708342492
  ![2019-01-30-10000-500](./2_ASStrategy/pnl_analysis/plot/pnl.png)

## Problems We Faced

### Crypto Data

The crypto data that we collected could was not properly parsed by Strategy Studio to be used in backtesting. When a backtest was run on the data there would be no sell orders in the orderbook and the bid price would have little to no movement.

Our solution was to use regular IEX data for our backtest for the meantime and look into how we could fix the crypto data.

### Positions Not Closed at EOD

In the PnL graphs there is a drop of PnL each day exactly at 20:00:00 and then return to the return to the regular level when the next trading day starts.

## Conclusion

### Project Reflection

**Xiaoyue Chen**

1. What did you specifically do individually for this project?

   - introduce some maket making strategies.
   - build the framework of python for the quantitative research in python.
   - write the C++ code for Avellanada-Stoikov market making model.

2. What did you learn as a result of doing your project?

   - I learned how to use strategy studio and the different data types in the high frequency trading.
   - Basic algorithmic trading strategies
   - I learned more about virtual machines, Linux, Yubikeys

3. If you had a time machine and could go back to the beginning, what would you have done differently?

   - start to learn shell command earlier and try to use StrategStudio on the Virtual Machine in advance to secure the backtest being carried out on time
   - not to chooose cryptocurrency as transaction object, try some trading object with both snapshoot data and strategy studio data.

4. If you were to continue working on this project, what would you continue to do to improve it, how, and why?

   - I want to construct the hyperparameter tunning daily for crypto to secure better parameter for trading.
   - try to implement some creative thoughts other than the methods existing.

5. What advice do you offer to future students taking this course and working on their semester-long project (besides "start earlier"... everyone ALWAYS says that). Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses.
   - try to get familiar with virtual machine first, learn the shell command and try some sample strategy(eg HelloWorld strategy)
   - try to tell snapshoot data, strategy studio and trade and quote data apart and prepare enough source of data for the final project.
   - do not try too complicated models, start from some easier one.

**Saranpat Prasertthum**

1. What did you specifically do individually for this project?

   - Getting all crypto data for the team. Develop a contaierize crypto data downloader and parser from Gate.io.
   - Install Strategy studio locall for debugging purposes.
   - Setting up Strategy studio on local machine.
   - Support other team and our team member on accessing VM and Strategy studio issue.
   - Wrote Simple test Strategies for Debugging Strategy studio.

2. What did you learn as a result of doing your project?

   - Learn how to use Strategy studio in detail
   - Learn how market data is processed in commercial backtesting.
   - Learn soft skill and understand team working dynamic.

3. If you had a time machine and could go back to the beginning, what would you have done differently?

   - I would not used cypto data as it not natively supported to use with Strategy studio.
   - I would look to previous year group project to get more ideas.
   - I would do more strategy writting because I spent too much time setting up Strategy studio for crypto data.

4. If you were to continue working on this project, what would you continue to do to improve it, how, and why?

   - I would like to implement ML strategy to the Strategy studio and create a better framework for backtesting as Pyblind method is not really efficient.
   - I would like to create my own api for easier backtesting.
   - Create more easy to read Strategy Studio Manual and how to set up for other group.

5. What advice do you offer to future students taking this course and working on their semester-long project (besides "start earlier"... everyone ALWAYS says that). Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses.
   - Prioritize teamwork, recognizing that collective effort and shared knowledge are key to team success.
   - Engage with professors through questions and actively participate in group chats for collaborative learning.

**Savya Raghavendra**

1. What did you specifically do individually for this project?

   - Researched various strategy's and wrote my own strategy
   - Helped other teammates with troubleshooting and testing as needed
   - Ran Backtests on VM
   - Listed/coded various methods to analyze performance of strategy

2. What did you learn as a result of doing your project?

   - I learned basics of developing strategies and backtesting on Strategy Studio
   - I learned the process of getting data, formatting it, uploading it, and then backtesting with it
   - I learned about yubikeys, VM's, terminal commands

3. If you had a time machine and could go back to the beginning, what would you have done differently?

   - I would focus on creating a well documented schedule with all the steps needed to take labeled clearly
   - I would focus on getting access to the VM, and learn how to navigate it
   - Getting and formatting data is also very important to do early
   - I would go through example strategies and just run them to get a feel for what to do

4. If you were to continue working on this project, what would you continue to do to improve it, how, and why?

   - I would create new Market Making models, and hypertune the parameters in Python to compare the PNL
   - I would also play around with Strategy Studio functionality to learn more

5. What advice do you offer to future students taking this course and working on their semester-long project (besides "start earlier"... everyone ALWAYS says that). Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses.
   - I would prioritize asking for help if you don't understand what you are supposed to do, because at least you will be trying to understand
   - I would also ensure you are understand why you are doing something rather than just blindly do it
   - Also, communicate with the team and if people are more knowledgeable than you in a certain area try to learn off them and teach them in your specialized area

**Nicholas Messina**

1. What did you specifically do individually for this project?

   - Wrote Order Flow Imbalance strategy and did analysis on results of backtests
   - Helped with troubleshooting bugs, and other teammates with backtesting and VM
   - Ran backtests on the VM

2. What did you learn as a result of doing your project?

   - I learned the basics of strategy studio/how a strategy might be created in industry
   - Basic algorithmic trading strategies
   - I learned more about virtual machines, Linux, Yubikeys

3. If you had a time machine and could go back to the beginning, what would you have done differently?

   - I would have got in the rhythm of working on the project consistenly for a set time each week
   - I would look back on past groups projects and build upon things that they created or follow their advice

4. If you were to continue working on this project, what would you continue to do to improve it, how, and why?

   - I would like to continue to make a more advanced stategy and learn more of the advanced parameters and functions that Strategy Studio provides.
   - Do hyperparameter tuning with Python and connect results with our strategy

5. What advice do you offer to future students taking this course and working on their semester-long project (besides "start earlier"... everyone ALWAYS says that). Providing detailed thoughtful advice to future students will be weighed heavily in evaluating your responses.
   - Make sure that the group gets all the technology problems (connecting to VM, learn how strategy is run, etc.) done first. Help each other to get set up
   - Take time to look at past groups work for things that can help you / examples on what to do.
   - Make small incremental goals and when you comeplete them make more.
   - Use Discord and don't be afraid to ask questions
